﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvenAndOdd
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter the first number: ");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter the last number: ");
            int lastNumber = Convert.ToInt32(Console.ReadLine());

            if (lastNumber < firstNumber)
            {
                Console.WriteLine("First Number shouldn't be greater than last number");
            }
            else if (firstNumber < 0 || lastNumber < 0)
            {
                Console.WriteLine("Please enter a valid positive integer");
            }
            else
            {
                List<int> odd = new List<int>();
                List<int> even = new List<int>();

                do
                {
                    if (firstNumber % 2 == 0)
                    {
                        even.Add(firstNumber);
                    }
                    else
                    {
                        odd.Add(firstNumber);
                    }
                    firstNumber++;
                } while (firstNumber <= lastNumber);

                Console.WriteLine("Even numbers: " + printNumbers(even));
                Console.WriteLine("Odd numbers: " + printNumbers(odd));
            }

            Console.WriteLine("\n\nPress any key to continue >>>");
            Console.ReadKey();
        }

        public static string printNumbers(List<int> a)
        {
            string output = "";
            for(int i = 0; i < a.Count(); i++)
            {
                output = output + a[i];
                if(i < a.Count() - 1)
                {
                    output = output + ", ";
                }
            }
            return output;
        }
    }
}
